s6-overlay-lite
===

An init system for containers, which does not require root
permissions, strongly inspired by
[s6-overlay](https://github.com/just-containers/s6-overlay).

It subscribes to the same philosophy of s6-overlay in terms of
containers running a tree of related processes. It does so without
using *s6-rc* however, and instead relying on the much simpler
*s6-svscan*. There is also no support for logging (i.e. processes
should log to stdout/stderr).

The only real requirement is a */run* tmpfs directory mounted with the
*exec* flag.

## Configuration

The init sequence works as follows:

* First, scripts in */etc/cont-init.d* are executed, in order. If any
  of them fails with a non-zero exit status, the container exits.

* Then, *s6-svscan* is started and will run services from
  */etc/services.d*. Each service is configured with a directory
  containing a *run* executable script, that should start the relevant
  daemon (without forking).
  
Services can be further subdivided in two categories:

* Primary services, that cause the whole container to exit when they
  terminate. To create one of these, the service directory should
  contain a *finish* script such as the following:
```
#!/bin/sh
exec s6-svscanctl -t /run/s6/service
```

* Secondary services, which will be automatically restarted by
  *s6-svscan* whenever they terminate.

### Optional services

One trick to control the execution of optional services dynamically
via environment variables is to have a *run* script such as the
following:

```
#!/bin/sh
if [ -n "$DISABLE_FOO" ]; then
    s6-svc -d /run/s6/service/foo
    exit 0
fi
# normal execution of the 'foo' daemon
exec foo
```

## Build system

The toolchain and build system is lifted straight from s6-overlay so
it maintains some of the same properties (e.g. statically linked, can
run on any base image, etc).

The differences are maintained in the *s6-overlay-build.patch*
file. They consist mostly of the *--tmpfsdir=/run/s6* setting, which
is used to avoid permission errors when *s6-linux-init* copies the
/etc/s6-overlay hierarchy over to /run (without being able to modify
/run itself, which is usually mounted with sticky permissions).

However instead of preparing tar.xz files for distribution, we
actually build a container image (based on Debian), and merge our
*layout* directory at that stage. The whole build process is handled
by the multi-stage Dockerfile.

## Usage

You can use the container image built by the CI for this repository as
a base image for your own. E.g., in a Dockerfile:

```
FROM registry.git.autistici.org/ai3/docker/s6-overlay-lite:master
...
```

The ENTRYPOINT is set to */init* by the base image and should not be
modified.
