FROM docker.io/library/debian:bookworm-slim AS build

ADD . /src
WORKDIR /src
RUN apt-get -q update && \
    apt-get -y upgrade && \
    apt-get -y install --no-install-recommends build-essential ca-certificates git make wget && \
    make -j4

FROM docker.io/library/debian:bookworm-slim

# Ensure we're building on top of the latest upgrades.
ENV DEBIAN_FRONTEND=noninteractive
RUN apt-get -q update && \
    apt-get -y upgrade && \
    apt-get clean && \
    rm -fr /var/lib/apt/lists/*

COPY --from=build /src/output/rootfs-overlay-x86_64-linux-musl/ /
COPY layout/ /
RUN ln -s /run/s6-env /etc/s6-overlay/env

# A/I customizations.
COPY deb_autistici_org.gpg /usr/share/keyrings/deb_autistici_org.gpg
COPY deb_autistici_org.list /etc/apt/sources.list.d/deb_autistici_org.list

ENTRYPOINT ["/init"]

# Ensure that if we run with another init, s6-telinit is
# provided with a runlevel.
# Ref: https://git.autistici.org/ai3/docker/s6-overlay-lite/-/issues/1
CMD 1

